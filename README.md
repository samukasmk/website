# Just Python Website


## Local setup:

- create a virtualenv: `python -m venv env`
- activate virtualenv: `source env/bin/activate`
- install lektor: `pip install lektor` or requirments: `pip install -r requirements.txt`
- access project folder: `cd web`
- run local server: `lektor server`
- access: `http://localhost:5000`
